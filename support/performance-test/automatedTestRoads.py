import os
from time import sleep

for file in os.listdir("configs"):
    if not file.endswith(".properties"):
        print "Skipping file: " + file
        continue
    
    conf = file.split(".")[0]
    
    #points
    pid = os.fork()
    
    if pid == 0:
        sleep(25)
        print "Test producer started"
        if os.system("python ~/scripts/testProducer.py \"data/input/input_roads5026.geojson\" -b \"broker01.streamslab.fer.hr:9092\" \"broker02.streamslab.fer.hr:9092\" \"broker03.streamslab.fer.hr:9092\" -t \"geospatial_index_in\"") != 0:
            raise Exception("Failed to run testProducer.py")
        
        exit()

    print "Spark job started"
    if os.system(
            "spark-submit --class hr.fer.ztel.geospatial.GeospatialIndexDistributed --master yarn --deploy-mode cluster --driver-cores 4 --num-executors 8 --executor-cores 2 --executor-memory 2G --files ~/configs/" + file + "  ~/jars/final_v3/geospatial-index-distributed-assembly-0.1.jar " + file) != 0:
        raise Exception("Failed to submit a job.")
    
    finished = os.waitpid(0, 0)
    print finished
    
    