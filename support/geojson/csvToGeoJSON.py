import csv, json
from geojson import Feature, FeatureCollection, Point

features = []
print("Poceo")
with open('input_csv.csv', newline='') as csvfile:
    reader = csv.reader(csvfile, delimiter=',')
    
    counter = 0
    for Accident_Index, Location_Easting_OSGR, Location_Northing_OSGR, Longitude, Latitude, Police_Force, Accident_Severity, Number_of_Vehicles, Number_of_Casualties, Date,Day_of_Week, Time, Local_Authority_District,Local_Authority_Highway,first_Road_Class,first_Road_Number,Road_Type,Speed_limit,Junction_Detail,Junction_Control,second_Road_Class,second_Road_Number,Pedestrian_Crossing_Human_Control,Pedestrian_Crossing_Physical_Facilities,Light_Conditions,Weather_Conditions,Road_Surface_Conditions,Special_Conditions_at_Site,Carriageway_Hazards,Urban_or_Rural_Area,Did_Police_Officer_Attend_Scene_of_Accident,LSOA_of_Accident_Location in reader:
        latitude, longitude, id = 0.0, 0.0, 0
  
        try:
            latitude, longitude = map(float, (Latitude, Longitude))
            id = int(Accident_Index)
        except:
            continue
        
        features.append(
            Feature(
                geometry = Point((longitude, latitude)),
                properties = {
                    'id': id
                }
            )
        )
        
        counter += 1
        if counter > 1000000:
            print("Kraj")
            break

collection = FeatureCollection(features)
with open("input1000000.geojson", "w") as f:
    f.write('%s' % collection)
    print("Zapisao")