import json
 
f = open("input_roads.geojson", "r")
o = open("input_roads1000.geojson", "w")
geo_objects = json.load(f)

counter = 0
for d in geo_objects["features"]:
    properties = {"id" : counter}
    d["properties"] = properties
    counter += 1

json.dump(geo_objects, o)