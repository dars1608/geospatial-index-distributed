package hr.fer.ztel.geospatial.stats

import java.text.SimpleDateFormat
import java.util.Date

import org.apache.spark.streaming.scheduler.BatchInfo

class BatchInfoWrapper(info: BatchInfo) {

  private val TIME_FORMAT = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss")
  private val date = new Date(info.batchTime.milliseconds)

  override def toString: String = s"${TIME_FORMAT.format(date)}\t${info.numRecords}\t${info.processingDelay.get}"

}
