package hr.fer.ztel.geospatial.stats

import org.apache.spark.streaming.scheduler._

import scala.collection.mutable.ListBuffer

class JobListener(batchInfos: ListBuffer[BatchInfoWrapper]) extends StreamingListener {
  override def onStreamingStarted(streamingStarted: StreamingListenerStreamingStarted): Unit = super.onStreamingStarted(streamingStarted)

  override def onReceiverStarted(receiverStarted: StreamingListenerReceiverStarted): Unit = super.onReceiverStarted(receiverStarted)

  override def onReceiverError(receiverError: StreamingListenerReceiverError): Unit = super.onReceiverError(receiverError)

  override def onReceiverStopped(receiverStopped: StreamingListenerReceiverStopped): Unit = super.onReceiverStopped(receiverStopped)

  override def onBatchSubmitted(batchSubmitted: StreamingListenerBatchSubmitted): Unit = super.onBatchSubmitted(batchSubmitted)

  override def onBatchStarted(batchStarted: StreamingListenerBatchStarted): Unit = super.onBatchStarted(batchStarted)

  override def onBatchCompleted(batchCompleted: StreamingListenerBatchCompleted): Unit = {
    val info = batchCompleted.batchInfo
    if (info.numRecords != 0) {
      batchInfos += new BatchInfoWrapper(info)
    }
  }

  override def onOutputOperationStarted(outputOperationStarted: StreamingListenerOutputOperationStarted): Unit = super.onOutputOperationStarted(outputOperationStarted)

  override def onOutputOperationCompleted(outputOperationCompleted: StreamingListenerOutputOperationCompleted): Unit = super.onOutputOperationCompleted(outputOperationCompleted)
}
