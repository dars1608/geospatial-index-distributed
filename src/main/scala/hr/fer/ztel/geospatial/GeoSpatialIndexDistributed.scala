package hr.fer.ztel.geospatial

import java.io.FileInputStream
import java.text.SimpleDateFormat
import java.util
import java.util.Date

import com.vividsolutions.jts.geom.Geometry
import hr.fer.ztel.geospatial.config.EnvironmentWrapper
import hr.fer.ztel.geospatial.config.IKeyWord._
import hr.fer.ztel.geospatial.data.SubscriptionPairModel
import hr.fer.ztel.geospatial.kafka.KafkaProducerWrapper
import hr.fer.ztel.geospatial.kafka.serializer.GeometryDeserializer
import hr.fer.ztel.geospatial.stats.{BatchInfoWrapper, JobListener}
import org.apache.kafka.clients.consumer.ConsumerConfig
import org.apache.kafka.common.serialization.LongDeserializer
import org.apache.log4j.{Level, Logger}
import org.apache.spark.broadcast.Broadcast
import org.apache.spark.serializer.KryoSerializer
import org.apache.spark.streaming.kafka010.ConsumerStrategies.Subscribe
import org.apache.spark.streaming.kafka010.KafkaUtils
import org.apache.spark.streaming.kafka010.LocationStrategies.PreferConsistent
import org.apache.spark.streaming.{Milliseconds, StreamingContext}
import org.apache.spark.{SparkConf, SparkContext}
import org.datasyslab.geospark.enums.{GridType, IndexType}
import org.datasyslab.geospark.formatMapper.GeoJsonReader
import org.datasyslab.geospark.serde.GeoSparkKryoRegistrator
import org.datasyslab.geospark.spatialOperator.JoinQuery
import org.datasyslab.geospark.spatialRDD.SpatialRDD

import scala.collection.JavaConverters._
import scala.collection.mutable.ListBuffer

object GeoSpatialIndexDistributed extends App {
  val env: EnvironmentWrapper = loadConfig(args)
  val batchInfos = new ListBuffer[BatchInfoWrapper]

  Logger.getLogger("org").setLevel(Level.toLevel(env.getProperty(LOGGING_LEVEL)))
  Logger.getLogger("akka").setLevel(Level.toLevel(env.getProperty(LOGGING_LEVEL)))
  Logger.getRootLogger.setLevel(Level.toLevel(env.getProperty(LOGGING_LEVEL)))

  val ssc = StreamingContext.getOrCreate(env.getProperty(CHECKPOINT_DIR), () => createSSC())
  val sc = ssc.sparkContext
  ssc.start()

  val timeout = env.getProperty(SPARK_STREAMING_TIMEOUT_SECONDS)
  if (timeout == null) {
    ssc.awaitTermination()
  } else {
    ssc.awaitTerminationOrTimeout(1000L * timeout.toLong)
  }

  Runtime.getRuntime.addShutdownHook(new Thread() {
    batchInfos.foreach(println)

    ssc.stop(stopSparkContext = false, stopGracefully = true)
    private val file = String.format("%s_CACHE_%s_SPATIAL_PARTITIONING_%s_SPATIAL_INDEX_%s_BATCH_TIME_%s",
      new SimpleDateFormat("yyyy_MM_dd_HH_mm_ss").format(new Date()),
      env.getProperty(SPARK_CACHE),
      env.getProperty(SPATIAL_PARTITIONING),
      env.getProperty(SPATIAL_INDEX),
      env.getProperty(SPARK_STREAMING_BATCH_TIME_MILLIS))

    private val res = sc
      .makeRDD(batchInfos)
      .repartition(1)
    res.saveAsTextFile(file)
  })


  def loadConfig(args: Array[String]): EnvironmentWrapper = {
    val env = new EnvironmentWrapper()

    if (args != null && args.length == 1) {
      env.load(new FileInputStream(args(0)))
    } else {
      env.load(this.getClass.getResourceAsStream(CONFIG_FILE_DEFAULT_LOCATION))
    }
    env
  }

  def createSSC(): StreamingContext = {
    val sparkConfig = new SparkConf()
      .setAppName(env.getProperty(SPARK_APP_NAME))
      .setMaster(env.getProperty(SPARK_MASTER_URL))
      .set("spark.serializer", classOf[KryoSerializer].getName)
      .set("spark.kryo.registrator", classOf[GeoSparkKryoRegistrator].getName)
      .set("spark.streaming.stopGracefullyOnShutdown", true.toString)
      .set("spark.streaming.backpressure.enabled", "true")

    val sc = SparkContext.getOrCreate(sparkConfig)
    val ssc = new StreamingContext(sc, Milliseconds(Integer.parseInt(env.getProperty(SPARK_STREAMING_BATCH_TIME_MILLIS))))
    ssc.addStreamingListener(new JobListener(batchInfos))

    val subscriptionsRDD = loadSubscriptions(sc)

    val topics = env.getProperty(STREAM_KAFKA_TOPICS_IN).replace(" ", "").split(",")
    val consumerConfig = Map[String, Object](
      ConsumerConfig.BOOTSTRAP_SERVERS_CONFIG -> env.getProperty(STREAM_KAFKA_BROKERS_IN),
      ConsumerConfig.GROUP_ID_CONFIG -> env.getProperty(STREAM_KAFKA_GROUP_ID),
      ConsumerConfig.KEY_DESERIALIZER_CLASS_CONFIG -> classOf[LongDeserializer].getName,
      ConsumerConfig.VALUE_DESERIALIZER_CLASS_CONFIG -> classOf[GeometryDeserializer].getName,
      ConsumerConfig.ENABLE_AUTO_COMMIT_CONFIG -> "false",
      ConsumerConfig.AUTO_OFFSET_RESET_CONFIG -> "latest"
    )

    val kafkaStreamIn =
      KafkaUtils.createDirectStream[Long, Geometry](
        ssc, PreferConsistent, Subscribe[Long, Geometry](topics, consumerConfig))

    val kafkaProducer: Broadcast[KafkaProducerWrapper[Long, SubscriptionPairModel]] = {
      sc.broadcast(KafkaProducerWrapper(
        env.getProperty(STREAM_KAFKA_BROKERS_OUT),
        env.getProperty(STREAM_KAFKA_GROUP_ID),
        env.getProperty(STREAM_KAFKA_TOPIC_OUT)))
    }

    kafkaStreamIn.foreachRDD(
      rdd => {
        if (!rdd.isEmpty()) {
          val inputRDD = new SpatialRDD[Geometry]
          inputRDD.rawSpatialRDD = rdd.map(c => c.value()).toJavaRDD()

          inputRDD.analyze()
          inputRDD.spatialPartitioning(
            GridType.getGridType(env.getProperty(SPATIAL_PARTITIONING)),
            env.getProperty(NUM_OF_SPATIAL_PARTITIONS).toInt
          )

          val indexType = env.getProperty(SPATIAL_INDEX)
          if (indexType != null) {
            inputRDD.buildIndex(IndexType.getIndexType(indexType), true)
          }

          subscriptionsRDD.spatialPartitioning(inputRDD.getPartitioner)
          if (indexType != null) {
            subscriptionsRDD.buildIndex(IndexType.getIndexType(indexType), true)
          }

          val result = JoinQuery.SpatialJoinQuery(inputRDD, subscriptionsRDD, indexType != null, true)
          result.foreach((geometryPair: (Geometry, util.HashSet[Geometry])) => {
            geometryPair._2.asScala.foreach((dataGeometry: Geometry) => {
              val dataId = dataGeometry.getUserData.asInstanceOf[String].toLong
              val subscriptionId = geometryPair._1.getUserData.asInstanceOf[String].split("\t")(0).toLong

              kafkaProducer.value.send(SubscriptionPairModel(dataId, subscriptionId))
            })
          })
        }
      }
    )

    ssc
  }

  def loadSubscriptions(sc: SparkContext): SpatialRDD[Geometry] = {
    val subscriptionsRDD = GeoJsonReader.readToGeometryRDD(
      sc,
      env.getProperty(SUBSCRIPTIONS_LOCATION),
      true,
      true
    )

    subscriptionsRDD
  }
}
