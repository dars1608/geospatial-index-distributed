package hr.fer.ztel.geospatial.kafka

import java.util.Properties
import java.util.concurrent.Future

import hr.fer.ztel.geospatial.kafka.serializer.SubscriptionPairSerializer
import org.apache.kafka.clients.producer.{KafkaProducer, ProducerConfig, ProducerRecord, RecordMetadata}
import org.apache.kafka.common.serialization.LongSerializer

class MessageProducerKafka[K, R](kafkaBroker: String, kafkaGroupId: String, val outTopic: String) extends Serializable {

  def createProducer(kafkaBroker: String, kafkaGroupId: String): KafkaProducer[K, R] = {
    val producerConfig = new Properties()
    producerConfig.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, kafkaBroker)
    producerConfig.put(ProducerConfig.CLIENT_ID_CONFIG, kafkaGroupId)
    producerConfig.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, classOf[LongSerializer].getName)
    producerConfig.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, classOf[SubscriptionPairSerializer].getName)

    new KafkaProducer[K, R](producerConfig)
  }

  val producer: KafkaProducer[K, R] = createProducer(kafkaBroker, kafkaGroupId)

  def send(model: R): Future[RecordMetadata] = producer.send(new ProducerRecord[K, R](outTopic, model))
}