package hr.fer.ztel.geospatial.kafka.serializer

import java.nio.charset.StandardCharsets
import java.util

import com.vividsolutions.jts.geom.Geometry
import org.apache.kafka.common.serialization.Deserializer
import org.datasyslab.geospark.enums.FileDataSplitter
import org.datasyslab.geospark.formatMapper.FormatMapper

class GeometryDeserializer extends Deserializer[Geometry] {

  private object $ {
    val FormatMapper : FormatMapper[Geometry] = new FormatMapper[Geometry](FileDataSplitter.GEOJSON, true)
  }

  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

  override def deserialize(topic: String, data: Array[Byte]): Geometry = $.FormatMapper.readGeoJSON(new String(data, StandardCharsets.UTF_8))

  override def close(): Unit = {}
}
