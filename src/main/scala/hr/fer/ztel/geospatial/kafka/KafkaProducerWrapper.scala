package hr.fer.ztel.geospatial.kafka

/**
 * Wrapper used for broadcasting KafkaProducer object throughout Spark distributed system.
 *
 * @param createProducer factory method for producing MessageProducerServiceKafka objects
 * @tparam K key type
 * @tparam R value type
 */
class KafkaProducerWrapper[K, R](createProducer: () => MessageProducerKafka[K, R]) extends Serializable {

  /* This is the key idea that allows us to work around running into
     NotSerializableExceptions. */
  lazy val producer: MessageProducerKafka[K, R] = createProducer()

  def send(data: R): Unit = {
    producer.send(data)
  }

}

/**
 * Factory object
 */
object KafkaProducerWrapper {

  def apply[K, R](kafkaBroker: String, kafkaGroupId: String, outTopic: String): KafkaProducerWrapper[K, R] = {
    val createProducerFunc = () => {
      val producerService = new MessageProducerKafka[K, R](kafkaBroker, kafkaGroupId, outTopic)

      sys.addShutdownHook {
        // Ensure that, on executor JVM shutdown, the Kafka producer sends
        // any buffered messages to Kafka before shutting down.
        producerService.producer.close()
      }

      producerService
    }
    new KafkaProducerWrapper[K, R](createProducerFunc)
  }
}