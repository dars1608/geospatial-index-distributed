package hr.fer.ztel.geospatial.kafka.serializer

import java.util

import hr.fer.ztel.geospatial.data.SubscriptionPairModel
import org.apache.kafka.common.serialization.Serializer

class SubscriptionPairSerializer extends Serializer[SubscriptionPairModel] {

  override def configure(configs: util.Map[String, _], isKey: Boolean): Unit = {}

  override def serialize(topic: String, data: SubscriptionPairModel): Array[Byte] = data.toString.getBytes

  override def close(): Unit = {}
}
