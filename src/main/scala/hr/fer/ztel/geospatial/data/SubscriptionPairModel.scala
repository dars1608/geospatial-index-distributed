package hr.fer.ztel.geospatial.data

@SerialVersionUID(1181641236459630175L)
case class SubscriptionPairModel(dataId : Long, subscriptionId : Long) extends Serializable {
  override def toString: String = s"""{"dataId": ${dataId}, "subscriptionId": ${subscriptionId} }"""
}
