package hr.fer.ztel.geospatial.config

case object IKeyWord {
  /**
   * Key for accessing the location of configuration file
   */
  val CONFIG_FILE_DEFAULT_LOCATION = "/config.properties"

  /**
   * Key for accessing the location of file which holds a list of subscriptions (in GeoJSON format)
   */
  val SUBSCRIPTIONS_LOCATION = "subscriptions.location"

  /**
   * Key for accessing the default Kafka group ID
   */
  val STREAM_KAFKA_GROUP_ID = "stream.kafka.groupId"

  /**
   * Key for accessing list of Kafka input stream brokers (host1:port1,host2:port2,...)
   */
  val STREAM_KAFKA_BROKERS_IN = "stream.kafka.brokers.in"

  /**
   * Key for accessing list of Kafka output stream brokers (host1:port1,host2:port2,...)
   */
  val STREAM_KAFKA_BROKERS_OUT = "stream.kafka.brokers.out"

  /**
   * Key for accessing the input stream topics
   */
  val STREAM_KAFKA_TOPICS_IN = "stream.kafka.topics.in"

  /**
   * Key for accessing the output stream topics
   */
  val STREAM_KAFKA_TOPIC_OUT = "stream.kafka.topic.out"

  /**
   * Key for accessing Sparks master URL
   */
  val SPARK_MASTER_URL = "spark.master.url"

  /**
   * Key for accessing the default Spark application name
   */
  val SPARK_APP_NAME = "spark.appName"

  /**
   * Key for accessing spatial partitioning strategy which will be used. Possible values are RTREE, QUADTREE
   */
  val SPATIAL_INDEX = "geospark.spatialIndex"

  /**
   * Key for accessing spatial index which will be used. Possible values are EQUALGRID, HILBERT, RTREE, QUADTREE, KDBTREE
   */
  val SPATIAL_PARTITIONING = "geospark.spatialPartitioning"

  /** Key for accessing cache enabled flag */
  val SPARK_CACHE = "spark.cache"

  /** Logging level */
  val LOGGING_LEVEL = "logging.level"

  /** Spark streaming timeout duration */
  val CHECKPOINT_DIR = "spark.streaming.checkpoint"

  /** Spark streaming batch time */
  val SPARK_STREAMING_BATCH_TIME_MILLIS = "spark.streaming.batchTime"

  /** Spark streaming timeout duration */
  val SPARK_STREAMING_TIMEOUT_SECONDS = "spark.streaming.timeout"

  /** Number of partitions which will be created from subscription dataset */
  val NUM_OF_SPATIAL_PARTITIONS = "geospark.numOfPartitions"
}
