package hr.fer.ztel.geospatial.config

import java.io.{IOException, InputStream}
import java.util
import java.util.{Objects, Properties}

class EnvironmentWrapper {
  private val DEFAULTS = new util.HashMap[String, String]()

  DEFAULTS.put(IKeyWord.SPARK_APP_NAME, "GeospatialIndexDistributed")
  DEFAULTS.put(IKeyWord.SUBSCRIPTIONS_LOCATION, "subscriptions.geojson")
  DEFAULTS.put(IKeyWord.SPARK_MASTER_URL, "local[4]")
  DEFAULTS.put(IKeyWord.SPATIAL_PARTITIONING, "QUADTREE")
  DEFAULTS.put(IKeyWord.SPATIAL_INDEX, null)
  DEFAULTS.put(IKeyWord.STREAM_KAFKA_BROKERS_IN, "localhost:9092")
  DEFAULTS.put(IKeyWord.STREAM_KAFKA_BROKERS_OUT, "localhost:9092")
  DEFAULTS.put(IKeyWord.STREAM_KAFKA_GROUP_ID, "geospatial_index")
  DEFAULTS.put(IKeyWord.STREAM_KAFKA_TOPICS_IN, "geospatial_index_in")
  DEFAULTS.put(IKeyWord.STREAM_KAFKA_TOPIC_OUT, "geospatial_index_out")
  DEFAULTS.put(IKeyWord.SPARK_CACHE, "false")
  DEFAULTS.put(IKeyWord.LOGGING_LEVEL, "DEBUG")
  DEFAULTS.put(IKeyWord.CHECKPOINT_DIR, "/tmp")
  DEFAULTS.put(IKeyWord.SPARK_STREAMING_BATCH_TIME_MILLIS, 1000.toString)
  DEFAULTS.put(IKeyWord.SPARK_STREAMING_TIMEOUT_SECONDS, null)
  DEFAULTS.put(IKeyWord.NUM_OF_SPATIAL_PARTITIONS, 2.toString)

  private val env = new Properties()

  @throws[IOException]
  def load(inputStream: InputStream): Unit = {
    env.load(inputStream)
  }

  def getProperty(keyWord: String): String = {
    Objects.requireNonNull(keyWord)
    if (!DEFAULTS.containsKey(keyWord)) throw new IllegalArgumentException(String.format("Key %s doesn't exist", keyWord))
    val p = env.getProperty(keyWord)
    if (p == null) DEFAULTS.get(keyWord)
    else p
  }
}
